#！/bin/bash
sudo chgrp www-data test.db
sudo chmod g+w test.db
sudo chgrp www-data laws.txt
sudo chmod g+w laws.txt

sudo chgrp -R www-data ./media/wordDict
sudo chmod -R g+w ./media/wordDict

sudo chgrp -R www-data /var/www/pricePredict/django_all.log
sudo chmod -R g+w /var/www/pricePredict/django_all.log

sudo chgrp -R ubuntu /var/www/pricePredict/django_all.log
sudo chmod -R g+w /var/www/pricePredict/django_all.log

sudo chgrp -R www-data /var/www/pricePredict/stopwords.txt
sudo chmod -R g+r /var/www/pricePredict/stopwords.txt
