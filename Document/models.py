from django.db import models
import django.utils.timezone as timezone

class Document(models.Model):
    #id = models.IntegerField(unique=True, primary_key=True)
    title = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    isUpload = models.BooleanField(db_column='isUpload')  # Field name made lowercase.
    add_date = models.DateTimeField(default = timezone.now)

    class Meta:
        managed = False
        db_table = 'Document'

class Spider_job(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    keyword = models.TextField()
    url = models.TextField()
    status = models.IntegerField()
    date = models.DateTimeField(default = timezone.now)
    num_case = models.IntegerField()

class Spider_task(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    url = models.TextField()
    keyword1 = models.TextField()
    keyword2 = models.TextField()
    keyword3 = models.TextField()
    classAttribute = models.TextField()
    idAttribute = models.TextField()
    pageTurning = models.TextField()
    pageStruct = models.TextField()
    knowledgeType = models.TextField()

class lexicon(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    word = models.TextField()
    num = models.TextField()
    prep = models.TextField()

