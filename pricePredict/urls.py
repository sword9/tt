"""pricePredict URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import view

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^welcome$', view.welcome),
    url(r'^Upload$', view.Upload),
    url(r'^upload_file/$', view.upload_file),
    url(r'^show_file/$', view.showFile),
    url(r'^file_download$', view.file_download),
    url(r'^testHtml$', view.testHtml),
    url(r'^fileView/$', view.fileView),
    url(r'^crawer$', view.crawer),
    url(r'^begin_to_craw$', view.begin_to_craw),
    url(r'^caseAnalysis$', view.caseAnalysis),
    url(r'^$', view.welcome),
    url(r'^jobView$', view.jobView),
    url(r'^lawAnalysis$', view.lawAnalysis),
    url(r'^addLawFile$', view.addLawFile),
    url(r'^CrawfileView$', view.CrawfileView),
    url(r'^participle$', view.participle),
    url(r'^add_setting$', view.add_setting),
    url(r'^batchCraw$', view.batchCraw),
    url(r'^show_task$', view.show_task),
    url(r'^dictUpload$', view.dictUpload),
    url('^participleResultRender', view.participleResultRender),
    url(r'^start/', view.app_start),
    url(r'^read/', view.Read_all_SQL),
    url(r'^edit/(.+)', view.Edit_lexicon),
    url(r'remove/', view.Remove_lexicon),
]

urlpatterns += staticfiles_urlpatterns()
