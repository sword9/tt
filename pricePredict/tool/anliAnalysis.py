#!/usr/bin/env python
# coding=utf-8
from gensim import corpora, models, similarities
import traceback
import jieba
import re
import sqlite3

import sys


def get_dictionary(documents):
    '''计算语料字典'''
    texts = [[word for word in jieba.cut(document)] for document in documents]
    dictionary = corpora.Dictionary(texts)   # 生成词典
    return dictionary,texts

def get_corpus(dictionary,texts):
    '''计算语料库在vsm上的tf表示'''
    corpus = [dictionary.doc2bow(text) for text in texts]
    return corpus

def get_tfidf_model(corpus):
    '''建立tfidf模型并计算语料库在该空间中的表示corpus_tfidf'''
    tfidf_model = models.TfidfModel(corpus)   # 建立tfidf模型
    corpus_tfidf = tfidf_model[corpus]
    return tfidf_model, corpus_tfidf

def get_lda_model(dictionary,corpus_tfidf,K):
    '''设定主题数为K，建立LDA模型并计算语料库在该空间的表示corpus_lda'''
    lda_model = models.LdaModel( corpus_tfidf, id2word=dictionary, num_topics = K )
    corpus_lda = lda_model[corpus_tfidf]
    return lda_model, corpus_lda

def get_lsi_model(dictionary,corpus_tfidf,K):
    '''设定主题数为K，建立LSI模型并计算语料库在该空间的表示corpus_lsi'''
    lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics = K )
    corpus_lsi = lsi_model[corpus_tfidf]
    return lsi_model, corpus_lsi

def get_tfidf_vec(vec,tfidf_model,_):
    return tfidf_model[vec]

def get_lda_vec(vec,tfidf_model,lda_model):
    return lda_model[tfidf_model[vec]]

def get_lsi_vec(vec,tfidf_model,lsi_model):
    return lsi_model[tfidf_model[vec]]

def get_sim(query,dictionary,corpus,model,get_query_vec,tfidf_model):
    '''计算待查询文本与语料的相似度'''
    query = [word for word in jieba.cut(query)]
    query_vec = get_query_vec(dictionary.doc2bow(query),tfidf_model,model)
    featureNum = len(dictionary.token2id.keys())   # 通过token2id得到特征数
    index=similarities.SparseMatrixSimilarity(corpus,num_features=featureNum)   #稀疏矩阵相似度，从而建立索引
    sim = index[query_vec]    # 计算最终相似度结果
    return sim

def merge_string(l):
    result = ""
    for item in l:
        result += item
    return result

def get_cases(query,algorithm,K):

    documents = []
    datas = []
    #连接数据库，获取所有历史案例数据作为语料
    conn = sqlite3.connect('/var/www/pricePredict/test.db')
    c = conn.cursor()
    cursor = c.execute("SELECT * from Document")
    for item in cursor:
        datas.append(item)
    documents = list(map(lambda x:x[1],datas))
    conn.close()

    #建立词典和预料集
    dictionary, texts = get_dictionary(documents)
    corpus = get_corpus(dictionary,texts)

    #建立tfidf基础模型和语料集在对应空间中的表示
    tfidf_model, corpus_tfidf = get_tfidf_model(corpus)

    #根据选择的算法不同计算出相似度数组
    if algorithm==0:
        model, corpus = tfidf_model, corpus_tfidf
        sim = get_sim(query,dictionary,corpus,model,get_tfidf_vec,tfidf_model)
    elif algorithm==1:
        model, corpus = get_lda_model(dictionary,corpus_tfidf,10)
        sim = get_sim(query,dictionary,corpus,model,get_lda_vec,tfidf_model)
    else:
        model, corpus = get_lsi_model(dictionary,corpus_tfidf,10)
        sim = get_sim(query,dictionary,corpus,model,get_lsi_vec,tfidf_model)

    #提取相似度最大的前K个值
    sim_list = list(reversed(sim.argsort()))[0:K]
    tmp = []
    for i in range(0,K):
        tmp.append(datas[sim_list[i]])

    #函数返回字典的数组
    ans = list(map(lambda x:{"title":x[0],"content":x[1],"url":x[2]},tmp))
    
    return ans

def justP():
    print("haha")
def get_rank_string(rank):
    if rank=='1':
        return u"省级"
    elif rank == '0':
        return u"国家级"
    else:
        return u"案例库不存在该法律"

def get_laws(query,algorithm,K):
    '''
    algorithm:
    0 : 用tfidf算法从法律库进行匹配
    1 : 用lda算法从法律库进行匹配
    2 : 用lsi算法从法律库进行匹配
    3 : 用lsi算法进行相似案例匹配并抽取案例中的适用法律
    '''
    print("hello")
    print(sys.argv[0])
    #从法律的文本文件中读取法律条文，并将法律库保存到documents中
    file = open('/var/www/pricePredict/laws.txt','r', encoding="utf-8")
    laws = []
    for item in file.readlines():
        laws.append(item)
    items = []
    for i in laws:
        if i[0]== '0' or i[0]=='1':
            items.append(i)
            
    #算法3是从案例中抽取法律，并从法律库查询具体内容，如果法律库中没有对应的法律，则返回的字典只有
    #法律名和第几条，级别的数据为-1
    if algorithm == 3:
        cases = get_cases(query,2,K)
        ans = []
        for case in cases:
            ans.append(re.findall(r'《[\u4e00-\u9fa5_a-zA-Z0-9（）《》<>-]+条',case['content']))            
        tmp = []
        for j in ans:
            for i in j:
                try:
                    law = re.findall(r'《[\u4e00-\u9fa5_a-zA-Z0-9（）<>-]+》',i)[0][1:-1]
                    index = re.findall(r'第[一二三四五六七八九十]+条',i)[0]
                    if (law,index) not in tmp:
                        tmp.append((law,index))
                except Exception as e:
                    print (e)
                    continue
        laws = []
        for (law,index) in tmp:   
            flag = False
            for d in items:
                if (law in d.split(' ')[1] or d.split(' ')[1] in law) and (index in d.split(' ')[2]):
                    laws.append({"rank":get_rank_string(d.split(' ')[0]),"law":d.split(' ')[1],"content":merge_string(d.split(' ')[2:])}) 
                    flag = True
            if not flag:
                laws.append({"rank":get_rank_string(-1),"law":law,"content":index}) 
        return laws
    
    #抽取法律文本内容
    documents = list(map(lambda x:merge_string(x.split(' ')[2:]),items))
    #建立词典和预料集
    dictionary, texts = get_dictionary(documents)
    corpus = get_corpus(dictionary,texts)

    #建立tfidf基础模型和语料集在对应空间中的表示
    tfidf_model, corpus_tfidf = get_tfidf_model(corpus)

    #根据选择的算法不同计算出相似度数组
    if algorithm==0:
        model, corpus = tfidf_model, corpus_tfidf
        sim = get_sim(query,dictionary,corpus,model,get_tfidf_vec,tfidf_model)
    elif algorithm==1:
        model, corpus = get_lda_model(dictionary,corpus_tfidf,10)
        sim = get_sim(query,dictionary,corpus,model,get_lda_vec,tfidf_model)
    else:
        model, corpus = get_lsi_model(dictionary,corpus_tfidf,10)
        sim = get_sim(query,dictionary,corpus,model,get_lsi_vec,tfidf_model)
        
    #提取相似度最大的前K个值
    sim_list = list(reversed(sim.argsort()))[0:K]
    #print (sim[sim_list])
    tmp = []
    for i in range(0,K):
        tmp.append(items[sim_list[i]])

    #函数返回字典的数组
    ans = list(map(lambda x:{"rank":get_rank_string(x.split(' ')[0]),"law":x.split(' ')[1],"content":merge_string(x.split(' ')[2:])},tmp))
    
    return ans
        

