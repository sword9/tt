#!/usr/bin/env python
# coding=utf-8
import urllib.request
import random
from bs4 import BeautifulSoup  
import random
import requests
import re
import sqlite3

my_headers=["Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36", 
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36", 
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0"
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14", 
            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)"    
            ] 

def is_file(url):
    file_name = ['doc','docx',
                 'xls','xlsx',
                 'ppt','pptx',
                 'pdf','rar',
                 'zip','mp4',
                 'gif','jpg',
                 'png','tif',
                 'wps',
                 'js', 'css']
    tmp = url.split('/')
    for name in file_name:
        if name in tmp[-1].lower():
            return True
    return False

def merge_url(array):
    url = ""
    for item in array:
        url += item+'/'
    return url

def normalize(url):  
    url = url.replace('/./','/')
    url = url.replace('//','/')
    url = url.replace('http:/','http://')
    return url

#返回网页html文件信息
def get_content(url,headers,proxy=""):  
    randdom_header = random.choice(headers)
    req=urllib.request.Request(url) 
    req.add_header("User-Agent",randdom_header) 
    req.add_header("GET",url)  
    content=urllib.request.urlopen(req).read() 
    return content

def match(string,keywords):
    for str in string:
        for keyword in keywords:
            if keyword in str:
                return True
    return False

def get_target(struct,keywords,soup,url,target_urls):
    text = soup.find_all(struct)
    for t in text:
        if match(t.strings,keywords):
            try:
                href = t.a.get('href')
                if href is None or href=="":
                    continue
                elif 'http' in href:
                    if baseurl in href:
                        target_url = href
                else:
                    if href[0]=='/':
                        target_url = baseurl + href
                    else:
                        tmp = url.split('/')
                        tmp.pop()
                        while(href[0]=='.'):
                            if href[1]=='.':
                                tmp.pop()
                                href = href[3:]
                            else:
                                href = href[2:]
                        if len(tmp)>=3:
                            target_url = normalize(merge_url(tmp)+href)
                        else:
                            target_url = normalize(baseurl + href)
                            
                if (t.strings,target_url) not in target_urls:
                    target_urls.append((t.strings,target_url))

            except:
                continue
       
    
def insert_into_cases(target_url,keywords,database_type):
    title = ""
    for str in target_url[0]:
        title += str
    title = title.replace('\n','').replace('\r','').replace('\t','')
    target_url = target_url[1]
    baseurl = target_url.split('/')[0]+'/'+target_url.split('/')[1]+'/'+target_url.split('/')[2]+'/'

    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    sql = "insert into cases (title,url,database_type,website) \
      VALUES ( {},{},{},{} )".format("'"+title+"'","'"+target_url+"'",database_type,"'"+baseurl+"'")
    c.execute(sql)
    conn.commit()
    conn.close()

def url2Document(website,class_="",id_=""):
    if website[0:4]!="http":
        website = "http://" + website
    if website[-1]!="/":
        website = website + "/"
    website = website.split('/')[0]+'/'+website.split('/')[1]+'/'+website.split('/')[2]+'/'
    
    #将cases表中要获取案例文本信息的元组取出并存在items列表中
    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    cursor = c.execute("SELECT title,url from cases where website='{}'".format(website))
    items = []
    for item in cursor:
        items.append(item)
    conn.close()

    #为了防止将重复的案例插入到Document表中，因此拿出所有案例网址，不重复插入
    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    cursor = c.execute("SELECT url from Document")
    urls = []
    for item in cursor:
        urls.append(item[0])
    conn.close()
    
    conn = sqlite3.connect('test.db')
    c = conn.cursor()

    for item in items:
        url = item[1]
        #已有数据的案例不重复插入
        if url in urls:
            continue
            
        strs = []
        try:
            html = get_content(url,my_headers)
            soup = BeautifulSoup(html,"html.parser",from_encoding='gb18030')
        except Exception as e:
            print (e)
            continue
    
        try:
            for str in soup.find_all('div',class_=class_,id=id_)[0].stripped_strings:
                strs.append(str)
        except Exception as e:
            print (e)
            continue
    
        content = ""
        for str in strs:
            content += str+'\n'
        content = content.replace('\n','').replace('\t','').replace('\r','').replace("'",'').replace('"','')

        sql = "insert into Document(id,title,content,url,isUpload,add_date)\
                values (null,{},{},{},{},datetime('now','localtime'))".format("'"+item[0]+"'","'"+content+"'","'"+item[1]+"'",0)
        c.execute(sql)
        conn.commit()
    conn.close()
    
def Spider_work(input_url,keywords,struct,nextPage,urls,target_urls):
    #获取网站根目录
    baseurl = input_url.split('/')[0]+'/'+input_url.split('/')[1]+'/'+input_url.split('/')[2]+'/'
    #建立已遍历的网址集合finished
    finished = []
    print("guanjiani    "+keywords[0])
    if (keywords[0] == "广东省发展"):
        print("equal")


    while(urls != []):
        ######################
        print (len(urls))
        print (len(finished))
        ######################
        url = normalize(urls.pop(0))
        if url in finished:
            continue
        finished.append(url)

        if is_file(url):
            continue
        #####################
        #print (url)
        #####################
        
        try:
            #proxy = work[random.randint(0,len(work)-1)]
            #while(not proxy_is_work(proxy)):
            #    proxy = work[random.randint(0,len(work)-1)]
            #    print proxy
            #html = get_content(url,my_headers,proxy)
            html = get_content(url,my_headers)
            soup = BeautifulSoup(html,"html.parser",from_encoding='gb18030') 
        except :
            #print url
            continue
        
        #get all urls in this page and add them into urls list
        for a in soup.find_all("a"):
            href = a.get('href')
            if href is None or href=="":
                continue
            elif 'http' in href:
                if baseurl in href:
                    u = href
                    if u not in urls and u not in finished:
                        urls.append(u)
            else:
                if href[0]=='/':
                    u = baseurl + href
                    if u not in urls and u not in finished:
                        urls.append(u)
                else:
                    tmp = url.split('/')
                    tmp.pop()
                    while(href!="" and href[0]=='.'):
                        if href[1]=='.':
                            tmp.pop()
                            href = href[3:]
                        else:
                            href = href[2:]
                    if len(tmp)>=3:
                        u = merge_url(tmp) + href
                        if u not in urls and u not in finished:
                            urls.append(u)
                    else:
                        u = baseurl + href
                        if u not in urls and u not in finished:
                            urls.append(u)
        
        if match(soup.stripped_strings,keywords):
            print ("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
            get_target(struct,keywords,soup,url,target_urls)
            if nextPage!="":
                tmp = url.split('/')
                tmp.pop()
                index = 0
                while(True):
                    index += 1
                    u = merge_url(tmp) + (nextPage % index)
                    try:
                        h = get_content(u,my_headers)
                        if u not in urls and u not in finished:
                            urls.append(u)
                    except :
                        break
    
def Spider(input_url,keywords,struct,database_type,nextPage="", class_="",id_=""):
    '''
    input_url为待爬取的目标网站
    keywords为关键词的list集合
    struct为网站中包含关键词的条目结构，如列表li或表格td
    database_type为构建库的类型，如新闻库，案例库等，传入参数为数字0,1,2
    nextPage为网站翻页结构，默认为空，如广东发改委结构为"index_%d.shtml"
    '''
    print("begin to spider1")
    print(input_url)
    print(keywords[0])
    print(struct)
    print(database_type)
    print(nextPage)
    if nextPage == "":
        print("空")
    if nextPage == None:
        print("none")
    print("guanjiani    "+keywords[0])
    if (keywords[0] == u"广东省发展"):
        print("equal")
    #将输入的网址处理成爬虫能处理的标准形式
    if input_url[0:4]!="http":
        input_url = "http://" + input_url
    if input_url[-1]!="/":
        input_url = input_url + "/"

    #建立待爬取的网址列表urls
    urls = []

    #建立包含关键词的网址集合target_urls
    target_urls = []
    #初始化将输入网址加入urls列表中
    urls.append(input_url)
    
    #在爬虫任务表中新插入一项任务
    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    cursor = c.execute("SELECT id  from Document_spider_job")
    max_id = 0
    for item in cursor:
        if item[0]>max_id:
            max_id = item[0]
    conn.commit()
    conn.close()
    key = ""
    for keyword in keywords:
        key = key + keyword + ', '
    conn = sqlite3.connect('test.db')
    c = conn.cursor()
    c.execute("insert into Document_spider_job(id,keyword,url,status,date,num_case)\
               values ({},{},{},0,datetime('now','localtime'),0)".format(max_id+1,"'"+key+"'","'"+input_url+"'"))
    conn.commit()

    try:
        #执行爬虫，爬虫执行结束后将本爬虫任务的状态修改成完成1
        Spider_work(input_url,keywords,struct,nextPage,urls,target_urls)
        target_urls = list(set(target_urls))
        print("len of target {}".format(len(target_urls)))
        count = 0
        for target_url in target_urls:
            try:
                insert_into_cases(target_url,keywords,database_type)
                count += 1
            except:
                count += 0
        
        conn = sqlite3.connect('test.db')
        c = conn.cursor()
        c.execute("update Document_spider_job set status=1 where id={}".format(max_id+1))
        c.execute("update Document_spider_job set num_case={} where id={}".format(count,max_id+1))
        conn.commit()
        url2Document(input_url, class_, id_)
        
    except Exception as e:
        print (e)
        #爬虫执行过程出错，爬虫任务结束并更改状态为失败-1
        conn = sqlite3.connect('test.db')
        c = conn.cursor()
        c.execute("update Document_spider_job set status=-1 where id={}".format(max_id+1))
        conn.commit()
    conn.close()

