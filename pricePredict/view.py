#coding=utf-8
import sys
sys.path.append("./pricePredict/tool")
print(sys.path[0])
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
import os
from Document.models import Document, Spider_job, Spider_task, lexicon
from django.core.paginator import Paginator
#from django.db import models
from django.http import FileResponse
import docx
from anliAnalysis import get_cases, get_laws, justP
from tasks import spider_asyn
from tasks import add
#from add_law import addLaw
from . import add_law
#from django.views.decorators.csrf import csrf_protect
from django.core.paginator import PageNotAnInteger
from django.core.paginator import EmptyPage
import json
import jieba
import logging
logger = logging.getLogger('file_out.views')

store_path = "/var/www/pricePredict/media/fileStore/"
law_path = "/var/www/pricePredict/media/lawFile/"

def testDb():
    for i in range(20):
        tmp = Document(title="这是标题", content="这是内容", url = "http://www.gddrc.gov.cn/zwgk/zdlyxxgkzl/jgzf/pgpt/201801/t20180111_459671.shtml")
        tmp.save()

def caseAnalysis(request):
    contacts = []
    if request.method == "POST":
        caseText = request.POST.get("caseText", None)
        caseNum = request.POST.get("caseNum", None)
        algorithmType = request.POST.get("algorithmType", None)
        if not caseText or not caseNum:
            return render(request, "callback.html", {"content": "缺少关键字段"})
        print(caseText)
        print(caseNum)
        print(algorithmType)
        contacts = get_cases(caseText, int(algorithmType), int(caseNum))
        #contacts = Document.objects.all()
        return render(request, "caseAnalysisResult.html", {"contacts":contacts})
    return render(request, "caseAnalysis.html")

# 进行分词并返回分词后的一个优先级字典
def jieba_func(sourceText, dictPath):
    if sourceText == None or dictPath == None:
        return "error"
    # 同步数据库lexicon到txt文件中
    items = lexicon.objects.all()
    if dictPath == "/var/www/pricePredict/media/wordDict/uploadDict.txt": 
        with open(dictPath, "w") as f:
            for item in items:
                print("item: "+item.word)
                m_str = ""
                if item.word is not None:
                    m_str += item.word
                if item.num is not None:
                    m_str = m_str + " " + item.num
                if item.prep is not None:
                    m_str = m_str + " " + item.prep
                m_str += "\n"
                f.write(m_str)

    jieba.load_userdict(dictPath)
    wordlist = jieba.cut(sourceText)
    #return " | ".join(wordlist)
    word_dict = {}
    stopwords = [line.strip() for line in open("/var/www/pricePredict/stopwords.txt").readlines() ]
    keywords = [line.split()[0].strip().replace('\ufeff', '') for line in open(dictPath).readlines() ]
    print(keywords)
    for i in wordlist:
        if i not in stopwords:
            word_dict[i] = word_dict.get(i, 0) + 1
    retlist = sorted(word_dict.items(), key = lambda d: d[1] if d[0] not in keywords else d[1]+1000, reverse=True)
    myRet = [[retlist.index(i)+1, i[0], i[1]] for i in retlist]
    return myRet

# 分词
jieba_retlist = []
jiebaText = ""
def participle(request):
    dictPath = "/var/www/pricePredict/media/wordDict/"
    if request.method == "POST":
        sourceText = request.POST.get("jiebaText", None)
        global jiebaText 
        jiebaText = sourceText
#        wordDict = request.FILES.get("ciku", None)
        options = request.POST.get("optionsRadios", None)
#        if wordDict == None and options == "option1":
#            return render(request, "callback.html", {"content":"error:使用专业词库时需要手动上传对应词库"})
#        if wordDict:
#            storeFile(dictPath, wordDict)
#        returnText = ""
        global jieba_retlist
        if options == "option1":
            jieba_retlist = jieba_func(sourceText, dictPath+"uploadDict.txt")
        elif options == "option2":
            jieba_retlist = jieba_func(sourceText, dictPath+"default.txt")
        else:
            return HttpResponse("error in options choice")
#       分页显示分词的统计结果
        return participleResultRender(request)
#        page_num = 1
#        return render(request, "participleResult.html", {"jiebaText":sourceText, "retlist": retlist}) 
    return render(request,"participle.html")

def participleResultRender(request):
    page_num = 1
    if 'page' in request.GET:
        page_num = request.GET['page']
    paginator = Paginator(jieba_retlist, 10)
    m_context = {}
    try:
        m_context['retlist'] = paginator.page(page_num)
    except PageNotAnInteger:
        m_context['retlist'] = paginator.page(1)
    except EmptyPage:
        m_context['retlist'] = paginator.page(paginator.num_pages)
    m_context["jiebaText"] = jiebaText
    return render(request, "participleResult.html", m_context)

# 在线编辑专业词典
def Read_all_SQL(request):
    obj_all = lexicon.objects.all()
    eaList = []
    for li in obj_all:
        eaList.append({"id":li.id, "word":li.word, 'num':li.num, "prep":li.prep})
    eaList_len = json.dumps(len(eaList))
    json_data_list = {'rows':eaList, 'total':eaList_len}
    easyList = json.dumps(json_data_list)
    return HttpResponse(easyList)

# 编辑数据并post提交到数据库
def Edit_lexicon(request, m_id):
    print('m_id1:-----------'+m_id)
    if request.method == "POST":
        print('m_id:-------'+m_id)
        word = request.POST.get('lexicon_word')
        num = request.POST.get('lexicon_num')
        prep = request.POST.get('lexicon_prep')
        dic = {'word':word, 'num':num, 'prep':prep}
        lexicon.objects.filter(id=m_id).update(**dic)
        return HttpResponse("Edit_ok")

# 增加数据与start_app
def app_start(request):
    if request.method == "POST":
        word = request.POST.get('lexicon_word')
        num = request.POST.get('lexicon_num')
        prep = request.POST.get('lexicon_prep')
        dic = {'word':word, 'num':num, 'prep':prep}
        print(dic)
        lexicon.objects.create(**dic)
        return render(request, 'cikuUpload.html')

# 移除数据
def Remove_lexicon(request):
    if request.method == "POST":
        id = request.POST.get('id')
        lexicon.objects.filter(id = int(id)).delete()
    return HttpResponse("REMOVE") 
    

# 上传专业词库uploadDict.txt
def dictUpload(request):
    dictPath = "/var/www/pricePredict/media/wordDict/"
    if request.method == "POST":
        wordDict = request.FILES.get("ciku", None)
        if wordDict == None:
            return render(request, "callbackBase2.html", {"content":"error: 上传不能为空"})
        storeFileWithName(dictPath, wordDict, "uploadDict.txt")
        # 将上传的关键词存储到数据库
        lexicon.objects.all().delete()
        with open(dictPath+"uploadDict.txt", "r") as f:
            for line in f.readlines():
                items = line.split()
                word, num, prep = "", "", ""
                if len(items) >= 1:
                    word = items[0].strip()
                if len(items) >= 2:
                    num = items[1].strip()
                if len(items) >= 3:
                    prep = items[2].strip()
                dic = {"word":word, "num":num, "prep":prep}
                lexicon.objects.create(**dic)
        render(request, "cikuUpload.html")        
        #return render(request, "callbackBase2.html", {"content":"上传成功"})
    return render(request, "cikuUpload.html")

def lawAnalysis(request):
    contacts = []
    if request.method == "POST":
        caseText = request.POST.get("caseText", None)
        caseNum = request.POST.get("caseNum", None)
        algorithmType = request.POST.get("algorithmType", None)
        if not caseText or not caseNum:
            return render(request, "callback.html", {"content": "缺少关键字段"})
        print(caseText)
        print(caseNum)
        print(algorithmType)
        justP()
        contacts = get_laws(caseText, int(algorithmType), int(caseNum))
        print(contacts)
        print("here")
        return render(request, "lawAnalysisResult.html", {"contacts":contacts})
    return render(request, "lawAnalysis.html")


def testHtml(request):
    #myfile = docx.Document("/home/sword/Desktop/test.doc")
    #returnS = []
    #for para in myfile.paragraphs:
    #    returnS.append(para.text)
    return render(request, 'caseAnalysis.html')

def welcome(request):
    add.delay()
    return render(request, "welcome.html")

def crawer(request):
    return render(request, "crawer.html")

def add_setting(request):
    if request.method == "POST":
        sites = request.POST.get("sites", None)
        keyword1 = request.POST.get("keywords1", None)
        keyword2 = request.POST.get("keywords2", None)
        keyword3 = request.POST.get("keywords3", None)
        pageTurnType = request.POST.get("pageTurnType", None)
        pageType = request.POST.get("pageType", None)
        knowledgeType = request.POST.get("knowledgeType", None)
        class_ = request.POST.get("class_", None)
        id_ = request.POST.get("id_", None)
        tmp = Spider_task(url = sites, keyword1 = keyword1, keyword2 = keyword2, keyword3 = keyword3,\
                           classAttribute = class_, idAttribute = id_, pageTurning = pageTurnType,\
                           pageStruct = pageType, knowledgeType = knowledgeType)
        tmp.save()
    tasks = Spider_task.objects.all()
    return render(request, "spiderTask.html", {"tasks":tasks})         

def show_task(request):
    tasks = Spider_task.objects.all()
    return render(request, "spiderTask.html", {"tasks":tasks})

def batchCraw(request):
    if request.method == "POST":
        checkList = request.POST.getlist("checkbox")
        print("----------------------------")
        print("size:")
        print(len(checkList))
        print(checkList[0])
        #print("size of checkList: &d"%(len(checkList)))
        for i in checkList:
            print(i)
         
        if checkList == None:
            print("empty checkList")
        if checkList:
            for newId in checkList:
                tasks = Spider_task.objects.filter(id=int(newId))
                print("task:")
                #print(task)
                keywords = []
                for task in tasks:
                    for i in [task.keyword1, task.keyword2, task.keyword3]:
                        if i:
                            keywords.append(i)
                    spider_asyn.delay(task.url, keywords, task.pageStruct, int(task.knowledgeType), task.pageTurning,\
                                       task.classAttribute, task.idAttribute)
        jobs = Spider_job.objects.all()
        return render(request, "spiderJob.html", {"jobs":jobs})

def begin_to_craw(request):
    if request.method == "POST":    # 请求方法为POST时，进行处理  
        sites = request.POST.get("sites", None)
        keywords1 = request.POST.get("keywords1", None)
        keywords2 = request.POST.get("keywords2", None)
        keywords3 = request.POST.get("keywords3", None)
        pageTurnType = request.POST.get("pageTurnType", "")
        pageType = request.POST.get("pageType", None)
        knowledgeType = request.POST.get("knowledgeType", None)
        class_ = request.POST.get("class_", None)
        id_ = request.POST.get("id_", None)
        print(sites+" a")
        print(keywords1)
        if not sites or not keywords1:
            return render(request, "callback.html", {"content": "缺少关键字段"})
        keywords = []
        for i in [keywords1, keywords2, keywords3]:
            if i:
                keywords.append(i)
        #add.delay()
        print("pageTurnType" + pageTurnType)
        spider_asyn.delay(sites, keywords, pageType, int(knowledgeType), pageTurnType, class_,id_)

        #提取出爬虫任务表
        jobs = Spider_job.objects.all()
        return render(request, "spiderJob.html", {"jobs": jobs})

def jobView(request):
    try:
        jobs = Spider_job.objects.all()
        return render(request, "spiderJob.html", {"jobs": jobs})
    except Exception as e:
        render(request, "callback.html", {"content": "列表为空"})

def fileView(request):
    request.encoding='utf-8'
    fileName = request.GET.get("anli", None)
    print(fileName)
    file_path = os.path.join(store_path, fileName)
    print(file_path)
    for root, dirs, files in os.walk(file_path):
        for file in files:
            print(file)
            if os.path.splitext(file)[-1] == ".docx" or os.path.splitext(file)[-1] == ".doc":
                print("Yes")
                myfile = docx.Document(os.path.join(file_path, file))
                returnS = []
                for para in myfile.paragraphs:
                    returnS.append(para.text)
                return render(request, 'fileView.html', {"title":fileName, "contents":returnS})
    return HttpResponse("file not found!")

def CrawfileView(request):
    request.encoding='utf-8'
    fileTitle = request.GET.get("anli",None)
    crawFile = Document.objects.filter(title=fileTitle)
    #if(len(crawFile) > 0)
    #    return HttpResponse("file not found !!")
    return render(request, 'CrawfileView.html', {"title":fileTitle, "url":crawFile[0].url, "content":crawFile[0].content})
    return HttpResponse("file not found")

def file_download(request):
    request.encoding='utf-8'
    file_name = request.GET.get("file_name", None)
    directory_name = request.GET.get("directory_name", None)
    if not file_name or not directory_name:
        return HttpResponse("no file")
    file=open(store_path + directory_name + "/" + file_name,'rb')  
    response =FileResponse(file)  
    response['Content-Type']='application/octet-stream'  
    response['Content-Disposition']='attachment;filename='+file_name  
    return HttpResponse("file not found!") 

def showFile(request):
    #doc_list = []
    #for i in range(20):
     #   doc_list.append(Document(title="哈哈", content="这是内容"))
    #testDb()
    try:
        doc_list = Document.objects.all().order_by("add_date")
        num = len(doc_list)
        print(doc_list[0].title)
        doc_list.reverse()
        print(doc_list[0].title)
        paginator=Paginator(doc_list, 20)
        page = request.GET.get('page')
        contacts = paginator.get_page(page)
        return render(request, 'list.html', {'contacts':contacts, 'Num': num })
    except Exception as e:
        return render(request, "callback.html", {"content": "列表为空"})

# 使用制定文件名存储文件
def storeFileWithName(path, myfile, filename):
    if not os.path.exists(path):
        os.mkdir(path)
    destination = open(os.path.join(path, filename), 'wb+')
    for chunk in myfile.chunks():
        destination.write(chunk)
    destination.close()


# 存储文件到指定路径
def storeFile(path, myfile):
    #path = path.decode("utf-8").encode("utf-8")
    if not os.path.exists(path):
        os.mkdir(path)
    destination = open(os.path.join(path, myfile.name),'wb+')    # 打开特定的文件进行二进制的写操作  
    for chunk in myfile.chunks():      # 分块写入文件  
        destination.write(chunk)  
    destination.close()

def Upload(request):
    return render(request, 'Upload.html')

def readWord(path, word_file):
    word_path = os.path.join(path, word_file.name)
    myfile = docx.Document(word_path)
    returnS = ""
    for para in myfile.paragraphs:
        returnS += para.text
    return returnS

#@csrf_protect   
def upload_file(request):  
    if request.method == "POST":    # 请求方法为POST时，进行处理  
        anliName = request.POST.get("anliName", None)
        if not anliName:
            return HttpResponse("案例名不能为空")
        anli = request.FILES.get("anli", None)    # 获取上传的文件，如果没有文件，则默认为None  
        fujian1 = request.FILES.get("fujian1", None)
        fujian2 = request.FILES.get("fujian2", None)
        fujian3 = request.FILES.get("fujian3", None)
        if not anli:  
            return HttpResponse("no files for upload!")
        storeFile(store_path+anliName, anli)
        # 将案例存到数据库中
        anliContent = readWord(store_path+anliName, anli)
        tmp = Document(title=anliName, content = anliContent, isUpload=True, url = "../fileView?anli="+anliName)
        tmp.save()
        if fujian1:
            storeFile(store_path+anliName, fujian1)
            
        if fujian2:
            storeFile(store_path+anliName, fujian2)
        if fujian3:
            storeFile(store_path+anliName, fujian3)
    return HttpResponseRedirect('../show_file/') 

def addLawFile(request):
    if request.method == "POST":
        lawFile = request.FILES.get("law", None)
        lawType = request.POST.get("lawType", None)
        if not lawFile:
            return render(request, "callback.html", {"content": "法律文本不能为空"})
        if not lawType:
            return render(request, "callback.html", {"content": "法律类型不能为空"})
        storeFile(law_path, lawFile)
        add_law.addLaw(law_path, lawFile.name, lawType)
        return render(request, "callback.html", {"content": "法律文本上传成功"})
    return render(request, "Upload_law.html")
