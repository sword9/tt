# coding=utf-8
"""
WSGI config for pricePredict project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from os.path import join,dirname,abspath
PROJECT_DIR = dirname(dirname(abspath(__file__)))


import sys

sys.path.insert(0,PROJECT_DIR)
sys.path.append('/var/www/pricePredict/pricePredict')
sys.path.append('/var/www/pricePredict/')


from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pricePredict.settings")

application = get_wsgi_application()
