# coding: utf-8  
import docx
import jieba
import sys

def loadText(file_path):
	file = docx.Document(file_path)
	return file.paragraphs

def isTitle(text):
	title_list = ["中华人民共和国","规定","条例","办法","规章","规则"]
	not_list = ["节"]
	size = min(len(text), 100)
	for i in title_list:
		if i in text[0:size]:
			for j in not_list:
				if j not in text[0:size]:
					return True
	return False

def isProvince(text):
	provincetext = "广东省"
	if provincetext in text:
		return "1 "+str(text)
	return "0 "+str(text)

# 查找前5个字里面是否包含”第“和”章“
def containZhang(text):
	text = text.strip()
	subText = text[0:5*3]
	if subText.find("第") != -1 and subText.find("章") != -1:
		return True
	return False

# 查找前5个字里面是否包含”第“和”条“
def containTiao(text):
	text = text.strip()
	subText = text[0:5*3]
	if subText.find("第") != -1 and subText.find("条") != -1:
		return True
	return False

def isFirstTiao(text):
	if "第一条" in text:
		return True
	return False

def printList(myList):
	for i in myList:
		print(i)

def readWord(file_path,file_name):
#file_path = file_path+file_name+".docx"
	file_path = file_path+file_name
	title_list = []
	zhang_list = []
	tiao_list = []
	lastSentence = ""
	for para in loadText(file_path):
		sentences = para.text.strip().split("\n")
		for sentence in sentences:
			if(containZhang(sentence) ):
				zhang_list.append(sentence)
				if lastSentence != "":
					tiao_list.append(lastSentence)
				lastSentence = ""
				continue
			if(containTiao(sentence)):
				if lastSentence != "":
					tiao_list.append(lastSentence)
				lastSentence = sentence
				continue
			if isTitle(sentence) and "CENTER" in str(para.alignment):
				title_list.append(sentence)
				lastSentence = ""
				continue
			if lastSentence != "":
				lastSentence += sentence
	if lastSentence != "":
		tiao_list.append(lastSentence)
	print(len(tiao_list))

	return title_list, tiao_list


def addLaw(file_path, file_name, label):
	'''
	读取file_path+file_name中的文件，将其追加到 /var/www/pricePredict/laws.txt 里面
	'''
	store_file_path = "/var/www/pricePredict/"
	title_list, tiao_list = readWord(file_path,file_name)
	tiaoIndex = 0
	with open(store_file_path + "laws.txt","a") as f:
		for i in title_list:
			if tiaoIndex < len(tiao_list):
				f.write(str(label)+" "+str(i)+" "+str(tiao_list[tiaoIndex])+"\n")
				tiaoIndex += 1
			while tiaoIndex < len(tiao_list) and not isFirstTiao(tiao_list[tiaoIndex]):
				f.write(str(label)+" "+str(i)+" "+str(tiao_list[tiaoIndex])+"\n")
				tiaoIndex += 1

if __name__ == "__main__":
	# argv[1]为文件路径，sys.argv[2]为文件名，sys.argv[3]为法律等级（国家或者省）
	addLaw(sys.argv[1], sys.argv[2], sys.argv[3])


	# 		#print("/".join(seg_list))






